About Me 

# SeanJohn Hoyle's GitLab README

I made this page to help others understand what it could be like to work with me, and to get a glimpse into who I am as a person. If you'd like to understand a particular topic or way I work, please add a coffee chat to my calendar. 

## Connect with me

<a href="https://www.linkedin.com/in/johnseankhoyleiv/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=for-the-badge&logo=LinkedIn&logoColor=white" /></a>
<a href="mailto:shoyle@gitlab.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white" /></a>
<a href="https://calendly.com/shoyle-gitlab/30meeting"><img align="left" src="https://img.shields.io/badge/Schedule a Meeting-purple?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>
<a href="https://gitlab.zoom.us/my/gitlab.seanhoyle"><img align="left" src="https://img.shields.io/badge/Zoom-brightgreen?&style=for-the-badge&logo=Google Calendar&logoColor=white" /></a>
&nbsp;

## About me

**Hello! My name is Sean Hoyle. I've been in DevOps for the past 11 years. I started at Rackspace Hosting and currently am a [Senior Customer Success Manager](https://about.gitlab.com/job-families/sales/customer-success-manager/) on the [Customer Success](https://about.gitlab.com/handbook/customer-success/) team.** 

 - I was born and raised in **San Antonio, Texas, United States** 🇺🇸.
 - I currently live with my wife and our three cats, Artemis 17, Arya 2 and Archer 1 😸.  
 - I'm currently improving my **Spanish**, so fingers crossed I'll be able to say what I mean... eventually. I'm always open to finding people to practice with if you happen to be fluent.
 - I'm an open book, feel free to ask me anything about myself and I'll most likely answer 📖.

## How you can help me

 - Please be as **open and honest with me** as you feel comfortable. I appreciate receiving any type of feedback and encourage building that trust early on. If you'd rather book a call to go over feedback I am always open to that as well.

## My working style

 - My timezone is [Central Standard Time](https://time.is/San_Antonio) and I'm usually online by **8:00am or 8:300am.**
 - I like keeping my afternoons open for deep work, and having calls and socialization in the mornings.
 - I consider myself an **Omnivert**, which pretty much means I can be either an introvert or extrovert depending on the circumstances. If you work closely with me you will probably see both pretty early on.
 - With that being said if you see me online and available, feel free to reach out for whatever reason, more than likely I am always down for a coffee chat.
 - I do get **distracted easily**, so sometimes I need to shut down notifications from slack and email to get my work done. Don't stress, I will get to answering them when I'm done!

## Communicating with me
 - I am open to any communication style, and I actually enjoy tailoring how we communicate with what works best for you. So the best bet is to just let me know how you'd like to chat and I guarantee I'll be open to anything.
 - I love **emojis** so feel free to use them to express any emotions when communicating with me 🙂.

## Strengths/Weaknesses

**Strengths:**
 - I'm a big advocate for teamwork and good communication. I love working across multiple teams and getting to know everyone. I can easily adjust to different working styles as well if you need me to.
 - If I don't know or understand something, I will do my best to figure it out myself. I'm big into research and enjoy getting lost in information trying to solve a problem.

**Weaknesses:**
 - Sometimes if I feel I am not a subject matter expert, I get a bit anxious to speak up even if I have an idea I think is worth sharing. It's something I am currently working on and one of my development goals.
 - I'm not always the best with zoom social cues, sometimes I will talk over other people by accident. I've been working on training myself to listen and pay attention more closely to avoid doing it going forward.

## Useful Links

* Notes/Instructions on how I set up my macOS development machine: [GitLab Workstation Setup Notes](https://gitlab.com/cwoolley-gitlab/cwoolley-gitlab/-/blob/main/gitlab-workstation-setup-notes.md)
* SeanJohn's Cheatsheet which has evolved from some of my onboarding notes: [GitLab CSM Cheatsheet](https://about.gitlab.com/handbook/customersuccessmanagement/development/dev/create/ide/csm-cheatsheet/)
* How I manage my Workload: [Coming Soon](website.com)

## Personality Test Insights
- Myers Briggs - [ENFP](https://www.16personalities.com/enfp-personality)
- [Strengths Finder](https://www.gallup.com/cliftonstrengths/en/252137/home.aspx)
  - [Woo](https://www.gallup.com/cliftonstrengths/en/252359/woo-theme.aspx) - Woo stands for winning others over. You enjoy the challenge of meeting new people and getting them to like you. Strangers are rarely intimidating to you. On the contrary, strangers can be energizing. You are drawn to them. You want to learn their names, ask them questions, and find some area of common interest so that you can strike up a conversation and build rapport. Some people shy away from starting up conversations because they worry about running out of things to say. You don’t. Not only are you rarely at a loss for words; you actually enjoy initiating with strangers because you derive satisfaction from breaking the ice and making a connection. Once that connection is made, you are quite happy to wrap it up and move on. There are new people to meet, new rooms to work, new crowds to mingle in. In your world there are no strangers, only friends you haven’t met yet—lots of them.
  - [Input](https://www.gallup.com/cliftonstrengths/en/252293/learner-theme.aspx) - You are inquisitive. You collect things. You might collect information—words, facts, books, and quotations—or you might collect tangible objects such as butterflies, baseball cards, porcelain dolls, or sepia photographs. Whatever you collect, you collect it because it interests you. And yours is the kind of mind that finds so many things interesting. The world is exciting precisely because of its infinite variety and complexity. If you read a great deal, it is not necessarily to refine your theories but, rather, to add more information to your archives. If you like to travel, it is because each new location offers novel artifacts and facts. These can be acquired and then stored away. Why are they worth storing? At the time of storing it is often hard to say exactly when or why you might need them, but who knows when they might become useful? With all those possible uses in mind, you really don’t feel comfortable throwing anything away. So you keep acquiring and compiling and filing stuff away. It’s interesting. It keeps your mind fresh. And perhaps one day some of it will prove valuable.
  - [Communication](https://www.gallup.com/cliftonstrengths/en/252185/communication-theme.aspx) - You like to explain, to describe, to host, to speak in public, and to write. This is your Communication theme at work. Ideas are a dry beginning. Events are static. You feel a need to bring them to life, to energize them, to make them exciting and vivid. And so you turn events into stories and practice telling them. You take the dry idea and enliven it with images and examples and metaphors. You believe that most people have a very short attention span. They are bombarded by information, but very little of it survives. You want your information—whether an idea, an event, a product’s features and benefits, a discovery, or a lesson—to survive. You want to divert their attention toward you and then capture it, lock it in. This is what drives your hunt for the perfect phrase. This is what draws you toward dramatic words and powerful word combinations. This is why people like to listen to you. Your word pictures pique their interest, sharpen their world, and inspire them to act.
  - [Individualization](https://www.gallup.com/cliftonstrengths/en/252134/achiever-theme.aspx) - Your Individualization theme leads you to be intrigued by the unique qualities of each person. You are impatient with generalizations or “types” because you don’t want to obscure what is special and distinct about each person. Instead, you focus on the differences between individuals. You instinctively observe each person’s style, each person’s motivation, how each thinks, and how each builds relationships. You hear the one-of-a-kind stories in each person’s life. This theme explains why you pick your friends just the right birthday gift, why you know that one person prefers praise in public and another detests it, and why you tailor your teaching style to accommodate one person’s need to be shown and another’s desire to “figure it out as I go.” Because you are such a keen observer of other people’s strengths, you can draw out the best in each person. This Individualization theme also helps you build productive teams. While some search around for the perfect team “structure” or “process,” you know instinctively that the secret to great teams is casting by individual strengths so that everyone can do a lot of what they do well.
  - [Strategic](https://www.gallup.com/cliftonstrengths/en/252185/communication-theme.aspx) - The Strategic theme enables you to sort through the clutter and find the best route. It is not a skill that can be taught. It is a distinct way of thinking, a special perspective on the world at large. This perspective allows you to see patterns where others simply see complexity. Mindful of these patterns, you play out alternative scenarios, always asking, “What if this happened? Okay, well what if this happened?” This recurring question helps you see around the next corner. There you can evaluate accurately the potential obstacles. Guided by where you see each path leading, you start to make selections. You discard the paths that lead nowhere. You discard the paths that lead straight into resistance. You discard the paths that lead into a fog of confusion. You cull and make selections until you arrive at the chosen path—your strategy. Armed with your strategy, you strike forward. This is your Strategic theme at work: “What if?” Select. Strike.
- Ennegram [Type ?](https://www.enneagraminstitute.com/)
- DISC ID & IS are about even. 
- [Tilt 365 - The Cross Pollinator](https://www.credly.com/org/tilt-365/badge/connection-tilt-cross-pollinator)

**Snapshot of Connection - The Cross Pollinator**

![ConnectionBanner.png](blob:file:///07934587-db00-4f93-97eb-b7f68bba47fd)

Tilting in CONNECTION means you are focused on connecting PEOPLE and IDEAS and have a natural intuition for reading other people's emotions. You know how to lean into their needs to give them precisely what they appear to want.


**The Cross Pollinator**

Your responses indicate that you have a natural proclivity for social and emotional intelligence that links People & Ideas. Adept at reading other people’s emotions and quickly surmising their specific interests, you are a natural cross pollinator of exciting ideas. Emotionally intuitive, you easily identify the subtle needs of others to give them precisely what they appear to want. Others often see you as very hospitable, gracious and generous. You are genuinely interested in diverse perspectives and want to perpetuate them in some way. Comfortable with going into a new situation without much information, you quickly interpret the cues and improvise easily. You may develop broad social networks that can be powerfully influential because your uplifting energy spreads human spirit and tempts others to join your causes and events.

**The Way You Show Up**

Your **Influencing Style** is inclusive and charismatic. Often a compelling persuader, you express your personal experiences with colorful stories and open transparency. Your **Top Character Strengths** are Inspiration, Ideation, Likability and Empathy, which combine to make you an attractive, enthusiastic and uplifting source of inspiration. Your **Unconscious Motivators** are to seek social approval and creative originality. You are happiest when you are appreciated by your social circle and free to explore your wide variety of interests without constraint. Your *De-Motivators* are people who are judgmental, critical or micromanaging, especially if they constrain you with rules or insist that you do tasks that are mundane or boring. Your **Preferred Pace** is quick and spur of the moment, so you tend to improvise in the moment to do what feels good right now but change course later if something better comes along.

**What Others Need to Know**

Your **Decision-Making Style** is quick yet noncommittal since you are known to second-guess your own choices after consulting your social connections and may change your mind several times before making a final decision. Your **Conflict Style** is to escape and withdraw your allegiance if anyone becomes a threat to your image socially. You believe social harmony is more important than strict interpretation of the rules. You **Want Others** to give you freedom, respect and approval so that everyone gets along and all ideas are heard. What You **Offer to Others** is a cheerful, approachable style that is always available to support, lift spirits and provide optimism.

**The Enlightened Cross Pollinator**

If you have a well-developed set of **Character Strengths** and live a well-rounded, fulfilled life where your contributions come from a **Generative** world mindset that extends beyond yourself, then you have learned the **Life Lesson** of this Tilt pattern and are probably making important contributions to the world around you. This mindset has less to do with age but more with how self-aware and healthy your internal compass has grown from a combination of healthy parenting, successful growth to adult independence, and continued growth of your creative intellect through life and work experiences. As an enlightened **Cross Pollinator**, your broad social influence, your childlike curiosity for learning, and your infectious enthusiasm can **Generate Viral Collaboration** that can truly change the world in important ways. If you are happy with where you are, the positive aspects of this profile will resonate most with you. However, even you will benefit from the insightful help in this report that extends beyond your surface patterns to underlying motivations that may surprise you. When the normal stresses of life unfold, or you end up stuck and wonder what is holding you back from your full potential, you will have the knowledge to proceed with self-awareness.

**The Cross Pollinator Under Stress**

**Under Stress**, you can be indecisive, self-doubting, and scattered. At such times, you might feel **overwhelmed** by the many people who seem to need you, on top of the numerous projects you have underway. This stress reaction can **Trigger Others** if you are irresponsible, noncommittal, superficial, overly talkative, or too transparent. The two underlying **Stress Emotions** that drive this uncertain nature in you are **Envy** and **Guilt**. You feel envious of what others have, indulge in pleasures too easily to assuage your appetite for more, then feel guilty about your lack of self-discipline. Your **Secret Fear** is the loss of reputation or image because that will mean you will be left out and seen as insignificant by your social circles. Deep down, you truly fear feeling that you are not special and not liked by others. This can cause you to feel sad and melancholy when you are alone. Your **Defense Mechanism** to this reaction is to make a great effort to be helpful, entertaining, generous, inclusive, and, most of all, busy giving too much to others at your own expense.

The **Faulty Story** this creates is that if you take care of everyone, they will never abandon you and understand how special you are. In this story, you stay in the past by coming across as superficially humble and escaping hard work by talking about what you’ll do in the future. The **Self-Fulfilling Prophecy** that results when you believe this story is that the more time you spend convincing instead of doing, the more they will start to rely on others, setting in motion the abandonment you most fear.

**What You May Need to Learn** during times of stress is that being busy doing too many things for too many people does not help you accomplish your most important goals. When you stop getting distracted by the plethora of great ideas you run across and find your deepest priority and purpose, you learn that you are capable of profound focus and concentration. You realize that you can be consistent and disciplined in showing that your productive behavior can accomplish the completion of big ideas that serve the masses, which means you will make true progress that astounds others.

**Your Path to Accelerate Potential**

Your **Healthy Story** is to realize that when your ego is busy running from one thing to the next, your real self is missing the present moment because you are not focused. Only when you are grounded in the present reality are you able to let go of an imagined past or future that feels overwhelming and commit to consistent practices that help you stay the course to completion. Then watch your creative output increase exponentially.

The **Two Big Moves** that bring you into balance and calm your fears about being alone and trapped in boredom are to:

**#1: Be Wise** as you learn to be more discerning, realistic, and focused on your top goals.

**#2: Be Bold** as you learn to set boundaries with others and assert your boundaries in relationships.

You will know you have arrived at your full creative potential when these two mantras are simple and easy for you.

————————


