## Tuesday, January 17th, 2023

## Daily Checklist
- [x] Create BlackBerry Cadence Call Issue 
- [x] Update [Sophie/Sean 1:1 Notes Doc ](https://docs.google.com/document/d/1j37Pz5UZGuN64cM1vSCD5_q74oYFVtsHp8H9_Two6J8/edit#)
- [x] Cancel Coffee chat with Justin
- [x] Reschedule coffee chat with Davis
- [x] Inform the Lulu Jenkins stand-up team you will not be attending
- [x] Message Sophie about the lulu Jenkins problem
- [x] Add Gainsight Update for lululemon Jenkins
