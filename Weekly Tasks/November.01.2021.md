## Week of November 1st, 2021 to November 5th, 2021



### Key Meetings:
 - [ ] 1:1 with Christiaan
 - [ ] Ally Weekly Meeting
 - [x] Iron Mountain Meeting 
 - [x] TAM Team Weekly
 - [x] SALSATAM Han,Billow,Hoyle Team Sync
 - [ ] Growth Coffee and Learn
 - [ ] Sync with Conley
 - [ ] Sync with Christen 
 - [ ] Async with Kevin on Lunch and Learn Scripts
 - [x] TAM Product Usage Data - Session 1
 - [x] TAM Product Usage Data - Session 2
 - [x] Named QBRs 11/04 

### Tasks for SALSATAM Teams:
 - [x] Add April to Ally Meeting on 11/02
 - [x] Add April Marks to all of our Slack Channels
 - [ ] Watch [Commit at Kubecon: DevOps 2021 - How to transform legacy IT into a z/OS DevOps team](https://www.youtube.com/watch?v=AcTw9m_5fgs)
 - [ ] Read through [GitLab Ultimate for Financial and Insurance](https://docs.google.com/presentation/d/1a6FLbVUmstfrPxQWWhRrMg4UfiDv2_cf-l8RXej3nI4/edit#slide=id.ge960ef3058_0_2678) Deck

### Other Tasks:
 - [ ] Onboarding Buddy work
 - [ ] Finish up October Manager Challenge

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] [Spanish Lessons](https://www.youtube.com/watch?v=LGMKg6MUdxI)
 - [ ] Prep for November Vacation
