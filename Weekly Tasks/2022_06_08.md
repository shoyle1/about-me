## 2022_06_08 

**Archer/RSA**

- [x] Call Prep
- [x] Gather Security Issues
- [ ] Ensure feedback is added to each issue
- [ ] Ensure PMs are tagged
- [ ] Create an escalation document 📃  
- [ ] License Overage
- [ ] Create meeting agenda 📔 📇 

**IRM security issues**

- [ ] Gather issues 
- [ ] Ensure feedback is in each issue 
- [ ] Ensure PMs are tagged 

**GE Aviation**
- [ ] Gather issues
- [ ] TAM Kickoff Deck
- [ ] Ensure PMs are tagged

**Bell Canada Feature Flag Prep**
- [ ] Review Feature Flag Docs
- [ ] Create a Demo in cloud under conley's project
