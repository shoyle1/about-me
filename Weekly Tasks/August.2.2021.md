## Week of August 2nd to August 6th, 2021

### Key Meetings:
 - [x] UX Weekly
 - [x] 1:1 with Jacki
 - [x] Acquisition Team Sync
 - [x] UX Showcase
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [ ] Expansion Optional Mid Milestone Catchup

### Tasks for Activation:
 - [ ] Write up Research Ideas for [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [x] Continue work with Nadia on next steps for[Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659).
 - [ ] Continue to clean up the [Experience Recommendations](https://gitlab.com/gitlab-org/growth/product/-/issues/1691) for the Verify Onboarding Scorecard.
 - [x] Fix up [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).

### Tasks for Expansion:
 - [x] Finish up additional flows for [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/).
 - [x] Continue working on [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).

### Other Tasks:
 - [ ] Sync up with Andy around the next UX Pairing group.
 - [x] Work away at the [New User Onboarding for Explorers](https://gitlab.com/gitlab-org/gitlab/-/issues/337258) 

### Personal Goals:
 - [x] Continue writing up my [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) as they relate to the [skills matrix](https://docs.google.com/spreadsheets/d/1tgA8SqVitI6MLFLWaCHtFJUa6x7CuuMiEwfEGAQfNyk/edit?usp=sharing).
 
### Issue Backlog:

**To Be Done**
- [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
- [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).

**Designs complete, waiting on implementation**
 - [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Allow users to invite others onto GitLab to help complete a task](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/393).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
