## Week of August 9th to August 13th, 2021

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Skip Level with Christie
 - [x] Donut Chat with Noria Aidam
 - [x] 1:1 with Matej
 - [x] Jacki's UX Team Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Chat About Invited User Onboarding
 - [x] Growth Engineering Weekly - APAC
 - [x] Donut Chat with Kevin
 - [x] Emily and Andy Design Pair

### Tasks for Activation:
 - [ ] Continue work on [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [x] Plan Sync Meeting with Nadia around [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659).
 - [ ] Continue to clean up the [Experience Recommendations](https://gitlab.com/gitlab-org/growth/product/-/issues/1691) for the Verify Onboarding Scorecard.
 - [x] Record Video of Experience for [Allow users to invite others onto GitLab to help complete a task](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/393).


### Tasks for Expansion:
 - [ ] Start looking at sythesis of [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
 - [x] Flush out modal for [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).
 - [x] Clean up issue for [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/)

### Other Tasks:
 - [x] Ongoing work around [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Make more [Growth L&D Recommendations](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/102). 

### Personal Goals:
 - [x] Urdu Lessons.
 - [x] Flush out [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) as they relate to the [skills matrix](https://docs.google.com/spreadsheets/d/1tgA8SqVitI6MLFLWaCHtFJUa6x7CuuMiEwfEGAQfNyk/edit?usp=sharing).
 
### Issue Backlog:

**To Be Done**
- [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).

**Designs complete, waiting on implementation**
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
