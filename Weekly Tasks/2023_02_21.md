## Week of February 21st, 2023 to February 24th, 2023

### Key Meetings:
 - [ ] ATB Premium Streams - Create Plan
 - [ ] GitLab | Haven Tech Bi-Monthly TAM Cadence
 - [ ] SALSATAM 💃 - Steve/Edmond/Sean Bi-Weekly
 - [ ] Strategic AMER CSMs 🎉
 - [ ] Growth Coffee and Learn 
 - [ ] Sean/Chloe 1:1
 - [ ] lululemon athletica GitLab training feedback meeting
 - [ ] lululemon- weekly project meeting
 - [ ] SAL-SA-TAM team call 💃Peter/Ron/Sean Weekly
 - [ ] Empower: Monthly TAM Call
 - [ ] AWS PartnerCast Series: AWS Cloud Practitioner Accelerator Program - Week 2 Office Hours

### Tasks for SALSATAM Teams:
 - [ ] Premium Workstreams 
 - [ ] Watch [Commit at Kubecon: DevOps 2021 - How to transform legacy IT into a z/OS DevOps team](https://www.youtube.com/watch?v=AcTw9m_5fgs)
 - [ ] Read through [GitLab Ultimate for Financial and Insurance](https://docs.google.com/presentation/d/1a6FLbVUmstfrPxQWWhRrMg4UfiDv2_cf-l8RXej3nI4/edit#slide=id.ge960ef3058_0_2678) Deck

### Top 3 Tasks:
 - [ ] HavenTech Call Prep
 - [ ] Schedule HavenTech Meetings 
 - [ ] IRM EBR Prep
 - [ ] IRM Ticket Follow-up Security Issues
 - [ ] Chloe Sean 1:1 notes 📝 
 - [ ] LuluLemon security issues
 - [ ] Read ATB Ultimate Chat


### Other Tasks:
 - [ ] ATB Premium Workstreams
 - [ ] Send Meeting Agendas to customers
 - [ ] post in slack about todays OH Call 📞
 - [ ] LuluLemon PM RoadMap Discussion - Cherry 🍒 messaged you in slack
 - [ ] 

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] [TODO](https://gitlab.com/shoyle1/todo)
 - [ ] [Spanish Lessons](https://www.youtube.com/watch?v=LGMKg6MUdxI)
 - [ ] Continue Value Stream Mapping Book - https://learning.oreilly.com/library/view/value-stream-mapping/9780071828918/series.html
