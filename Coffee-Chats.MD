## My Coffee Chats
Wanted to keep track of all the Coffee and Donut chats I've been able to have while at GitLab.

Thanks to [Austin Regnery](https://gitlab.com/aregnery) for this fantastic idea.

|Person|Role|Last Chat|Count|
|------|----|----|-----|
|[David Thomas](https://gitlab.com/dt) 🇺🇸|Solution Architect|`2019/10/30`|☕️|
|[Simon Uhegbu](https://gitlab.com/suhegbu) 🇬🇧|Technical Account Manager|`2019/10/30`|☕️|
|[Jonathan Schafer](https://gitlab.com/aregnery) 🇺🇸|Product Designer|`2019/11/07`|☕️|
|[Jamie Reid](https://gitlab.com/jrreid) 🇨🇦|Technical Account Manager|`2019/11/06`|☕️|
|[Michael Leopard](https://gitlab.com/leopardm) 🇺🇸|Professional Services Engineer|`2019/11/12`|☕️|
|[Sarah Daily](https://gitlab.com/sdaily) 🇺🇸|Senior Marketing Operations Manager|`2019/11/13`|☕️|
|[Andrew Newdigate](https://gitlab.com/andrewn) 🇸🇮|Distinguished Engineer, Infrastructure|`2019/11/13`|☕️|
|[Edmond Chan ](https://gitlab.com/edmondchan) 🇨🇦|Solutions Architect|`2019/11/07`|☕️|
|[Blake Chalfant-Kero](https://gitlab.com/bchalfantkero) 🇺🇸|SMB Account Executive|`2019/11/08`|☕️|
|[Evan Mathis](https://gitlab.com/emathis) 🇺🇸|SDR - Public Sector|`2019/11/12`|☕️|
|[Darwin Sanoy](https://gitlab.com/DarwinJS) 🇺🇸|Solutions Architect|`2019/11/15`|☕️|
|[Mark Robinson](https://gitlab.com/mrobinson) 🇺🇸|Federal Channel Partner and SI Manager|`2019/11/18`|☕️|
|[Yolanda Feldstein](https://gitlab.com/yofeldstein) 🇺🇸|Technical Account Manager|`2019/11/14`|☕️|
|[Taylor Medlin](https://gitlab.com/taylor) 🇺🇸|Solutions Architect|`2019/11/19`|☕️ |
|[Susan Elizabeth Hill](https://gitlab.com/susanhill) 🇺🇸|Sr. Sourcer|`2019/11/20`|☕️|
|[Jordan Goodwin](https://gitlab.com/jordan.goodwin) 🇺🇸|Senior Sales Account Leader|`2019/11/21`|☕️|
|[Darren Murph](https://gitlab.com/dmurph) 🇺🇸|Head of Remote|`2019/11/21`|☕️|
|[Amber Stahn](https://gitlab.com/Astahn) 🇺🇸|Sales Operations|`2019/12/03`|☕️|
|[Sara Ahbabou](https://gitlab.com/sahbabou) 🇺🇸|Support Engineer|`2019/11/29`|🧠☕️|
|[Mohammed Al Kobaisy](https://gitlab.com/malkobaisy) 🇺🇸|IT Operations System Administrator|`2019/12/17`|☕️|
|[Heather Simpson](https://gitlab.com/pcalder) 🇺🇸|Sr. External Communications Analyst, Security|`2019/12/06`|☕️|
|[Jesse Rabbits](https://gitlab.com/jrabbits) 🇺🇸|Manager, Deal Desk|`2019/12/06`|☕️|
|[Kelly Murdock](https://gitlab.com/lmurdock) 🇺🇸|Sr. Manager, Recruiting and Strategic Programs|`2020/03/12`|☕️|
|[Anthony Carella](https://gitlab.com/acarella) 🇺🇸|SDR|`2019/04/20`|☕️|
|[Charlie Ablett](https://gitlab.com/cablett) 🇳🇿|Senior Backend Engineer, Plan|`2021/04/21`|☕️|
|[Ashley Jameson](https://gitlab.com/ashjameson) 🇺🇸|People Operations Specialist|`2020/09/22`|🧠☕️|
|[Tan Le](https://gitlab.com/Tle) 🇦🇺|Backend Engineer|`2020/05/21`|🍩|
|[Alejandro Michell](https://gitlab.com/amichell) 🇺🇸|Technical Account Manager|`2021/05/25`|🍩|
|[Michelle Harris](https://gitlab.com/mharris3) 🇺🇸|Sr. Program Manager, Customer Programs|`2021/06/21`|☕️|
|[Sam Morris](https://gitlab.com/sam) 🇺🇸|Technical Account Manager|`2021/06/29`|☕️|
|[Justin Mandell](https://gitlab.com/jmandell) 🇺🇸|Product Design Manager|`2021/07/14`|☕️🍩|
|[Austin Regnery](https://gitlab.com/aregnery) 🇺🇸|Senior Product Designer|`2021/07/07`|☕️🍩|
|[Emily McInerney](https://gitlab.com/emcinerney) 🇺🇸|Manager Customer Success Operations|`2021/07/27`|☕️|
|[Wil Spillane](https://gitlab.com/wspillane) 🇺🇸|Head of Social Marketing|`2021/07/30`|🧠🍩☕️|
|[Brian Cupin](https://gitlab.com/wspillane) 🇺🇸|Technical Account Manager|`2021/07/30`|☕️|
|[Susan Tacker](https://gitlab.com/stacker) 🇺🇸|Sr Mgr, Technical Writing|`2021/08/12`|🍩☕️|
|[Becka Lippert](https://gitlab.com/beckalippert) 🇺🇸|Product Designer|`2021/08/12`|🍩☕️|
|[Robert Kohnke](https://gitlab.com/rkohnke) 🇺🇸|Marketing Strategy & Performance Data Analyst|`2021/08/27`|🧠🍩☕️|
|[Anne Lasch](https://gitlab.com/alasch) 🇺🇸|Sr UX Researcher|`2021/08/31`|☕️🍩|
|[Manuel Kraft](https://gitlab.com/manuel.kraft) 🇩🇪| Technical Account Manager|`2021/08/30`|☕️|
|[Matt Salik](https://gitlab.com/msalik) 🇺🇸| Senior Brand Design|`2021/08/30`|☕️|
|[Dallas Reedy](https://gitlab.com/dreedy) 🇺🇸|Fullstack Engineer|`2021/06/01`|☕️|
|[Katie Wilkinson](https://gitlab.com/Katie.Wilkinson) 🇺🇸|Sales Development Representative|`2021/09/23`|🍩|
|[Andrew Volpe](https://gitlab.com/andyvolpe) 🇺🇸|Sr. Product Designer - Secure|`2021/09/01`|☕️🍩|
|[Osnat Vider](https://gitlab.com/ovider) 🇩🇪| Technical Account Manager|`2021/09/20`|☕️|
|[Ashley Jameson](https://gitlab.com/ashjameson) 🇺🇸|People Operations Specialist|`2021/10/05`|🧠🍩☕️|
|[Anastasiia Bakunovets](https://gitlab.com/bakunovets) 🇷🇺|Sales Development Representative|`2021/10/11`|🍩|
|[Christie Lenneville](https://gitlab.com/clenneville) 🇺🇸|VP of User Experience|`2021/10/11`|🍩☕️|
|[Sunjung Park](https://gitlab.com/sunjungp) 🇩🇪|Senior Product Designer|`2021/10/12`|☕️🍩|
|[Amanda Rueda](https://gitlab.com/amandarueda) 🇲🇽|Senior Product Manager|`2021/11/19`|☕️🍩|
|[Matt Salik](https://gitlab.com/msalik) 🇺🇸|Senior Brand Design|`2021/11/22`|☕️🍩|
|[Cody Pritchard](https://gitlab.com/cpritchard1) 🇺🇸|Technical Account Manager|`2021/12/01`|☕️🍩|
|[Katie Macoy](https://gitlab.com/katiemacoy) 🇳🇿|Product Designer|`2021/12/01`|☕️🍩|
|[Stelios Batzogiannis](https://gitlab.com/SteliosB) 🇦🇺|Sales Development Representative|`2021/12/02`|🍩☕️|
|[Kevin Vogt](https://gitlab.com/kvogt1) 🇺🇸|Professional Services Engineer|`2021/12/17`|☕️|
|[Amy Qualls](https://gitlab.com/aqualls) 🇺🇸|Senior Technical Writer|`2021/12/17`|☕️|
|[Justin Mandell](https://gitlab.com/jmandell) 🇺🇸|Product Design Manager: Configure & Monitor, and Secure & Growth teams|`2022/11/02`|🍩☕️|
|[Anastasia McDonald](https://gitlab.com/a_mcdonald) 🇳🇿|Senior Software Engineer in Test, Create:Source Code|`2022/11/09`|🍩☕️|
|[PJ Metz](https://gitlab.com/PjMetz) 🇺🇸|Education Evangelist|`2022/11/10`|☕️|
|[Laura Meckley](https://gitlab.com/lmeckley) 🇺🇸|Frontend Engineer, Fulfillment::Billing & Subscription Mgmt|`2022/11/10`|🍩☕️|
|[Carrie Kroutil](https://gitlab.com/ckroutil) 🇺🇸|Fullstack Engineering Manager,Package: Package Registry|`2022/11/28`|☕️|
