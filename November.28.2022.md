## Week of November 28th, 2022 to December 2nd, 2022

### Key Meetings:
 - [ ] CSM Office Hours
 - [ ] ATB POV Planning
 - [ ] Iron Mountain Meeting 
 - [ ] TAM Team Weekly
 - [ ] SALSATAM Han,Billow,Hoyle Team Sync
 - [ ] Growth Coffee and Learn
 - [ ] Sync with Conley
 - [ ] Sync with Christen 
 - [ ] Async with Kevin on Lunch and Learn Scripts
 - [ ] TAM Product Usage Data - Session 1
 - [ ] TAM Product Usage Data - Session 2
 - [ ] Named QBRs 11/04 

### Tasks for SALSATAM Teams:
 - [ ] Reschedule lulu TAM call and add Ravi
 - [ ] Send Jayson GitLab Security Scanners info on why we chose the tools we did
 - [ ] Update 1:1 Notes


### Other Tasks:
 - [ ] Begin Kubernetes Training
 - [ ] Finish up October Manager Challenge

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] [Spanish Lessons](https://www.youtube.com/watch?v=LGMKg6MUdxI)
 - [ ] Prep for December Vacation
